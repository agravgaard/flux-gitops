# Dashboard sources

Flux:
 * [cluster](https://github.com/fluxcd/flux2/blob/main/manifests/monitoring/monitoring-config/dashboards/cluster.json)
 * [control-plane](https://github.com/fluxcd/flux2/blob/main/manifests/monitoring/monitoring-config/dashboards/control-plane.json)

Node-problem-detector:
 * [npd](https://grafana.com/grafana/dashboards/15549-node-problem-detector/)
