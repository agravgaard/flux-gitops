## Create secret values

Initial encryption
```bash
cat ~/age.agekey \
  | grep -i public \
  | sed 's/^.*: //' \
  | xargs -I mykey sops \
    --age=mykey \
    --encrypt \
    --encrypted-regex '^config$' \
    --in-place secret-values.yaml
```

To edit `secret-values.yaml` use:
```bash
export SOPS_AGE_KEY_FILE=$HOME/age.agekey
sops secret-values.yaml
```
