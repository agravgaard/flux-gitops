## Setup

For each node make sure the given path is available, e.g. `/var/data`
(remember the root filesystem is not writable on Talos, but `/var` is)

The drive can be wiped with the following Pod:
```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: disk-wipe
spec:
  restartPolicy: Never
  nodeName: worker11
  containers:
  - name: disk-wipe
    image: busybox
    securityContext:
      privileged: true
    command: ["/bin/sh", "-c", "dd if=/dev/zero bs=1M count=100 oflag=direct of=/dev/nvme0n1"]
```
By applying, wait for success, and cleaning it up again:
```bash
kubectl apply -n node-problem-detector -f disk-wipe.yaml
kubectl wait --timeout=900s --for=jsonpath='{.status.phase}=Succeeded' -n node-problem-detector pod disk-wipe
kubectl delete -n node-problem-detector pod disk-wipe
```
The `node-problem-detector` namespace was chosen here because it only enforces privileged pod security policies

It can then be reformatted automatically (as XFS) with Talos by adding a partition mountpoint:
```yaml
---
machine:
  network:
    hostname: worker11
  disks:
      - device: /dev/nvme0n1 # The name of the disk to use.
        partitions:
          - mountpoint: /var/data
```
