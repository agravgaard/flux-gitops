# How to set up cloudflared tunnel config

First install cloudflared locally:
```bash
pacman -S cloudflared
```

Run:
```bash
cloudflared tunnel login
```
and authorize a tunnel for the desired hostname, e.g. "cyberbunny.cyou".
This generates `~/.cloudflared/cert.pem`.

Now create a tunnel with the name you want, e.g.:
```bash
cloudflared tunnel create turing-pi
```
This generates the needed credentials files, the command output tells you where, but likely in `~/cloudflared`.

Generate a kubernetes secret with the content of the file as the value of `credentials.json`, e.g.:
```bash
kubectl create secret generic cloudflare-tunnel-secret \
  --from-file=credentials.json=$HOME/.cloudflared/4d6cb998-ab48-4efb-8939-bb0636cad099.json \
  --dry-run=client -o yaml > cloudflared-secret.yaml
```

A kubernetes secret id just base64 encoded, not encrypted, so let's encrypt:
```bash
cat ~/age.agekey \
  | grep -i public \
  | sed 's/^.*: //' \
  | xargs -I mykey sops \
    --age=mykey \
    --encrypt \
    --encrypted-regex '^data$' \
    --in-place cloudflared-secret.yaml
```
See the REAME in the root dir for more info on age and sops.

Set the tunnel and secret name in the Helm values:
```yaml
spec:
  values:
    cloudflare:
      secretName: cloudflare-tunnel-secret
      tunnelName: "turing-pi"
```

Finally, configure the DNS in cloudflare by adding a cname for the subdomain(s) pointing to `<UUID>.cfargotunnel.com`.
(You can get the UUID in several ways, e.g. `cloudflared tunnel list`)
