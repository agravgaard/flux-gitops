## Create and encrypt the CloudFlare API Token Secret

```bash
kubectl create secret generic cloudflare-api-token \
  -o yaml --dry-run=client \
  --from-literal=api-token="Your CloudFlare API Key here" \
  > cf-api-key.yaml
```

```bash
cat ~/age.agekey \
  | grep -i public \
  | sed 's/^.*: //' \
  | xargs -I mykey sops \
    --age=mykey \
    --encrypt \
    --encrypted-regex '^data$' \
    --in-place ./cf-api-key.yaml
```
