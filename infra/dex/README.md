
Initial encrytion:
```bash
cat ~/age.agekey \
  | grep -i public \
  | sed 's/^.*: //' \
  | xargs -I mykey sops \
    --age=mykey \
    --encrypt \
    --encrypted-regex '^client(ID|Secret)$' \
    --in-place \
    secret-config.yaml
```

Edit with `sops edit secret-config.yaml`
