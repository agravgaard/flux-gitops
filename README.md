# Flux GitOps configuration for my personal K3s cluster

I'd recommend you fork the project if you want to use it yourself.

## Prerequisites

Edit and run the preparation script. It will label nodes create secrets,
```bash
./prepare.sh
```

## To apply the configuration

```
export GITLAB_TOKEN=$(cat ~/turing-pi-flux.token)

flux bootstrap gitlab --owner agravgaard --repository flux-gitops --branch main --path clusters/turing-pi --token-auth
```

## Mark nodes to use turing-pi-maintainer (i2c management)

This step is also performed by the `prepare.sh` script.
If your node names are worker#, where # is 1 to 7:
```
./enable-sdm.sh
```

Otherwise, manually:
```
kubectl label node <INSERT NODE NAME> smarter-device-manager=enabled
```

## Grafana login

```
username: admin
password: prom-operator
```
(See: https://fluxcd.io/docs/guides/monitoring/)

## Secrets (for Dex/GitLab integration)

Rewrite [dex-helm.yaml](./clusters/turing-pi/dex-helm.yaml) with
your [GitLab App ID and Secret](https://docs.gitlab.com/ee/integration/oauth_provider.html#user-owned-applications).
and remove the sops section at the bottom.

Install sops and age, e.g.:
```bash
yay -S sops rust-rage
```

Follow [the flux secret instructions](https://fluxcd.io/flux/guides/mozilla-sops/#encrypting-secrets-using-age):
```bash
rage-keygen -o ~/age.agekey
```
Create secret:
```bash
cat ~/age.agekey \
  | kubectl create secret generic sops-age \
    --namespace flux-system \
    --from-file=age.agekey=/dev/stdin
```
Encrypt file:
```bash
cd ./clusters/turing-pi/

cat ~/age.agekey \
  | grep -i public \
  | sed 's/^.*: //' \
  | xargs -I mykey sops \
    --age=mykey --encrypt \
    --encrypted-regex '^client(ID|Secret)$' \
    --in-place dex-helm.yaml
```
