#!/bin/bash

for i in {1..7}
do
  kubectl label node "worker$i" smarter-device-manager=enabled
done
