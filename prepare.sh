#!/bin/bash

set -eux -o pipefail

# Create namespace for flux and its decryption key
kubectl create namespace flux-system

# Create decryption key secret for flux
kubectl create secret generic sops-age \
	--namespace flux-system \
	--from-file=age.agekey=/dev/stdin <~/age.agekey

# Label nodes 1-7 for i2c
#for i in {1..7}; do
#	kubectl label node "worker$i" smarter-device-manager=enabled
#done
