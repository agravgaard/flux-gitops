#!/bin/bash

for i in {8..11}
do
  kubectl label node "worker$i" storage=enabled
done
